const categoryData = [
  {id: 0, name: 'Laptop', createdDate: '2018/01/01', status: 'Active'},
  {id: 1, name: 'Smartphone', createdDate: '2018/01/01', status: 'Active'},
  {id: 2, name: 'Fashion', createdDate: '2018/02/01', status: 'Active'},
  {id: 3, name: 'Test', createdDate: '2018/02/01', status: 'Active'},
  {id: 4, name: 'Book', createdDate: '2018/03/01', status: 'Active'},
  {id: 5, name: 'Book', createdDate: '2018/03/01', status: 'Active'},
  {id: 6, name: 'Book', createdDate: '2018/03/01', status: 'Active'},
  {id: 7, name: 'Book', createdDate: '2018/03/01', status: 'Active'},
  {id: 8, name: 'Book', createdDate: '2018/03/01', status: 'Active'},
  {id: 9, name: 'Book', createdDate: '2018/03/01', status: 'Active'},
  {id: 10, name: 'Book', createdDate: '2018/03/01', status: 'Active'},
  {id: 11, name: 'Book', createdDate: '2018/03/01', status: 'Active'},
  {id: 12, name: 'Book', createdDate: '2018/03/01', status: 'Active'},
  {id: 13, name: 'Book', createdDate: '2018/03/01', status: 'Active'},
  {id: 14, name: 'Book', createdDate: '2018/03/01', status: 'Active'},
  {id: 15, name: 'Book', createdDate: '2018/03/01', status: 'Active'},
]

export default categoryData
